#!ruby
# -*- mode: ruby; coding: UTF-8 -*-

require 'date'

dict = {}

Dir.glob("**/.indexes.rbm") do |index_file|
  dir = File.dirname(index_file)
  index = Marshal.load(File.read(index_file))
  index.each do |k, v|
    key = [dir, k.sub(/\.md$|\.rst$/, ".html")].join("/").sub(/^\.?\/?/, "/")
    dict[key] = v["title"]
  end
end

if File.exist?(".meta_additional_index.rbm")
  dict.merge! Marshal.load(File.read ".meta_additional_index.rbm")
end

ARGF.each do |line|
  line.gsub!(%r!\bhttps?://\S+|\b/\S+!) do
    str = $&.sub(%r!^https?://[^\/]*!, "").sub(/\?.*/, "").sub(/\/$/, "")
    dict[str] || str
  end
  puts line
end