#!ruby
# -*- mode: ruby; coding: UTF-8 -*-

require 'date'
require 'optparse'

dict = {}
mode = :normal

LOG_FORMAT =  Regexp.new(%q!^(?<ipaddr>[0-9A-Fa-f:.]*) (?<luser>\S+) (?<rsuer>\S+) (?<accesstime>\[[^\]]*\]) (?:"(?<proto>(?:GET|POST|DELETE|PUT|OPTIONS|HEAD)) (?<file>[^"]*) HTTP/(?<protovar>[0-9.]+)" (?<status>[0-9]{3}) [0-9]+ "(?<ref>[^"]*)"|".*?") "(?<ua>[^"]*)"!)

opts = OptionParser.new
opts.on("-n") { mode = :total }
opts.on("-u") { mode = :uu }
opts.parse!

Dir.glob("**/.indexes.rbm") do |index_file|
  dir = File.dirname(index_file)
  index = Marshal.load(File.read(index_file))
  index.each do |k, v|
    key = [dir, k.sub(/\.md$|\.rst$/, ".html")].join("/").sub(/^\.?\/?/, "/")
    dict[key] = v["title"]
  end
end

if File.exist?(".meta_additional_index.rbm")
  dict.merge! Marshal.load(File.read ".meta_additional_index.rbm")
end


case mode
when :normal
  count = Hash.new(0)
  ARGF.each do |line|
    line =~ /GET ([^ ]+)/ or next
    path = $1.sub(/\?.*/, "").sub(/\/$/, "")
    path = dict[path] || path
    count[path] += 1
  end

  count.sort_by {|k, v| v}.reverse_each do |i|
    puts i.reverse.join("\t")
  end
when :total
  count = 0
  ARGF.each do |line|
    match = LOG_FORMAT.match(line)
    unless match
      abort line
    end
    next unless match[:proto]
    #next unless match[:proto] == "GET"
    next unless match[:status][0] == "2" || match[:status][0] == "304"
    count += 1 if dict[match[:file]]
  end

  puts count
when :uu
  users = Hash.new
  ARGF.each do |line|
    users[line[%r:^\S+:]] = true
  end
  puts users.size
end
