#!ruby

replace = ARGV[0] || "/"
dict = {}

Dir.glob("**/*") do |fn|
  next unless File.file? fn
  content = File.read fn
  begin
    if content =~ %r:\<title.*?\>(.*?)\</title:
      title = $1.gsub(/\<.*?\>/, "")
      dict[fn.sub(/^\.?\/?/, replace)] = title
    end
  rescue
    next
  end
end

Marshal.dump(dict, STDOUT)
